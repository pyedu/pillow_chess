Creating a chess board with Pillow
===================================

You need to convert the image to square JPEG
and run this python script.

I have not used command line arguments, so you need to change the file
names in the file.

For details see the doc string in chess.py
