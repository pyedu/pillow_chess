#!/usr/bin/env python3
"""
Based on these
https://pillow.readthedocs.io/en/stable/reference/ImageDraw.html
https://stackoverflow.com/a/21768191/813946

You can resize & convert the picture on Linux with imagemagick like that

convert -resize '200x200!' earth.png earth.jpg
It must be in jpg format.
"""
from PIL import Image, ImageDraw

with Image.open("earth.jpg") as im:

    draw = ImageDraw.Draw(im, "RGBA")
    x, y = im.size
    dx, dy = x/8, y/8
    x0, y0 = 0, 0
    odd = [1,3,5,7]
    even = [0,2,4,6]
    lines = [even, odd] * 4

    for dy_factor, line in enumerate(lines):
        y0 = 0 + dy_factor * dy
        for dx_factor in line:
            x0 = 0 + dx_factor * dx
            draw.polygon([(x0, y0), (x0+dx, y0), (x0+dx, y0+dy), (x0, y0+dy)], (255, 255, 255, 125))


    # write to stdout
    # im.save(sys.stdout, "PNG")
    im.save("chess.png")

